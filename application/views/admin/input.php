<div class="container-fluid">
<div class="card">
    <div class="card-body">
        <?= form_open_multipart('admin/input'); ?>
        <form method="POST" action="<?= base_url('admin/input'); ?>">
            <div class="form-group row">
                <label for="email" class="col-sm-2 col-form-label">Nama</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="email" name="nama" value="">
                </div>
            </div>
            <div class="form-group row">
                <label for="name" class="col-sm-2 col-form-label">Username</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="name" name="username" value="">
                </div>
            </div>
            <div class="form-group row">
                <label for="name" class="col-sm-2 col-form-label">Password</label>
                <div class="col-sm-10">
                    <input type="password" class="form-control" id="password" name="password" value="">
                </div>
            </div>
            <div class="form-group row">
                <label for="name" class="col-sm-2 col-form-label">Jabatan</label>
                <div class="col-sm-10">
                    <select class="form-control" name="jabatan">
                        <option value="Teknisi">Teknisi</option>
                        <option value="BJP">Kepala BJP</option>
                        <option value="Perkakas">Kalab Perkakas</option>
                        <option value="CNC">Kalab CNC</option>
                        <option value="Konstruksi">Kalab Konstruksi</option>
                        <option value="UjiBahan">Kalab UjiBahan</option>
                        <option value="NonMetal">Kalab NonMetal</option>
                        <option value="UPIK3L">Kepala UPIK3L</option>
                        <option value="Admin">Admin</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-2">TTD</div>
                <div class="col-sm-10">
                    <div class="row">
                        <div class="col-sm-3">
                            <img src="" class="img-thumbnail">
                        </div>
                        <div class="col-sm-9">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="image" name="ttd">
                                <label class="custom-file-label" for="image">Choose file</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group row justify-content-end">
                <div class="col-sm-10">
                    <input type="submit" name="submit" class="btn btn-primary" value="Input">
                </div>
            </div>
        </form>
        </div>
    </div>
</div>
