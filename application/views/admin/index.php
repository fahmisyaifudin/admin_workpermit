        <!-- Begin Page Content -->
        <div class="container-fluid">
          <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
          <div class="card mb-3">
          <div class="card-body">
          <a href="<?= base_url('admin/input'); ?>"><button class="float-right btn btn-primary mb-2" > Add User</button></a>
          <table class="table">
          <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Username</th>
                <th scope="col">Jabatan</th>
                <th scope="col">Action</th>
              </tr>
          </thead>
          <tbody>
              <?php

              foreach ($user -> result_array() as $value) {
                echo '  <tr>
                          <th scope="row">'.$value['id'].'</th>
                          <td>'.$value['Nama'].'</td>
                          <td>'.$value['username'].'</td>
                          <td>'.$value['Jabatan'].'</td>
                          <td><a href="'.base_url("admin/edit/".$value['id']).'"><button class="btn btn-success">Edit</button></a> <span class="mr-1"></span>
                            <a href="'.base_url("admin/delete/".$value['id']).'"><button class="btn btn-danger">Delete</button></a></td>
                        </tr>';
              }

            ?>
          </tbody>
          </table>
        </div>
      </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

     