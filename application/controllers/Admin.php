<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	public function index(){
		$data['user_now'] = $this->db->get_where('user', ['username' => 'Admin'])->row_array();
		$data['user'] = $this->db->get('user');
		$data['title'] = "Dashboard";
		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);
		$this->load->view('admin/index', $data);
		$this->load->view('templates/footer', $data);
	}
	public function edit(){
		$data['user_now'] = $this->db->get_where('user', ['username' => 'Admin'])->row_array();
		$data['user'] = $this->db->get_where('user', ['id' => $this->uri->segment('3')])->row_array();
		$data['title'] = "Edit User";
		if ($this->input->post('submit')) {
			$edit = [
					'Nama' => $this->input->post('nama'),
					'Jabatan' => $this->input->post('jabatan'),
					'username' => $this->input->post('username'),
					'ttd' => $this->input->post('ttd')

				];

			//var_dump($this->input->post('topup'));
		
			$this->db->update('user', $edit, array('id'=>$this->input->post('id')));
			redirect('admin');
		}else{
		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);
		$this->load->view('admin/edit', $data);
		$this->load->view('templates/footer', $data);
		}
		
	}
	public function input(){
		$this->load->helper('form');
		$data['user_now'] = $this->db->get_where('user', ['username' => 'Admin'])->row_array();
		$data['title'] = "Input User";
		if ($this->input->post('submit')) {
			if($_FILES['ttd']['name']){
				$config['upload_path']          = 'assets/img/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = '2048';
                $this->load->library('upload', $config);
                if($this->upload->do_upload('ttd')){
                	$ttd = $this->upload->data('file_name'); 
                }else{
                	$ttd = 'defaultttd.jpg';
                }
			$user = [
					'Nama' => $this->input->post('nama'),
					'Jabatan' => $this->input->post('jabatan'),
					'username' => $this->input->post('username'),
					'password' => $this->input->post('password'),
					'ttd' => $ttd
				];
			$this->db->insert('user', $user);
			redirect('admin');
		}

		}else{
		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('templates/topbar', $data);
		$this->load->view('admin/input', $data);
		$this->load->view('templates/footer', $data);
		}

	}
	public function delete(){
		$this->db->delete('user', array('id' => $this->uri->segment('3')));
		redirect('admin');
	}
	public function logout()
    {
        redirect('http://localhost/workpermit/proseslogout.php');
    }


}