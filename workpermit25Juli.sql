-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 25 Jun 2019 pada 02.46
-- Versi Server: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `workpermit`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `bengkel`
--

CREATE TABLE IF NOT EXISTS `bengkel` (
`ID` int(20) NOT NULL,
  `Bengkel` varchar(20) NOT NULL,
  `PeralatanYangDipakai` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `bengkel`
--

INSERT INTO `bengkel` (`ID`, `Bengkel`, `PeralatanYangDipakai`) VALUES
(1, 'Konstruksi', ',Mesin Las SMAW'),
(2, 'CNC', ',CNC Milling');

-- --------------------------------------------------------

--
-- Struktur dari tabel `peralatan`
--

CREATE TABLE IF NOT EXISTS `peralatan` (
  `PeralatanYangDipakai` varchar(50) NOT NULL,
  `Hazard` longtext NOT NULL,
  `RiskControl` longtext NOT NULL,
  `Equipment` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `peralatan`
--

INSERT INTO `peralatan` (`PeralatanYangDipakai`, `Hazard`, `RiskControl`, `Equipment`) VALUES
('3 Roll Machine', 'Material yang berat,Listrik Bertegangan Tinggi,Tombol OFF tidak berfungsi,Benda berputar,Debu\r\n', 'Memberi petunjuk peggunaan tombol emergency,Memastikan isolasi listrik terpasang dengan sempurna,memastikan jalan menuju mesin bebat halangan/hambatan,Memberi peringatan peggunaan tombol bahaya listrik,Menggunakan baju bengkel yang sesuai,tidak berdiri dibawah crane pada saat pengangkatan,Memasang tombol Emergency,Menggunakan alat angkat jika benda terlalu berat,Melakukan perawatan mesin dan grounding secara teratur,Turunkan drive roll dengan hati-hati,Membuat jarak aman dari mesin,Membuat tanda area aman,Menggnakan alat angkat jika material terlalu berat,Membuat tanda area aman,Melakukan perawatan mesin secara teratur\r\n\r\n\r\n\r\n', 'Sarung tangan,Wearpack,Safety Shoes,Masker'),
('CNC Laser Cutter', 'Listrik Bertegangan Tinggi,Tekanan berlebih,Material yang tajam,Panas dari laser,Asap dan gas beracun,Mesin yang beroprasi,Material yang panas', 'Memastikan isolasi listrik terpasang dengan sempurna,perawatan mesin dan grounding secara teratur,Memberi peringatan peggunaan tombol bahaya listrik,Mengecek regolator compressor,Pastikan area kerja bersih dari minyak oli dan bahan yang mudah terbakar,Jangan melakukan pemotongan pada container yang berisi cairan atau gas yang mudah terbakar,Pastikan mesin belum beroperasi pada ssaat pemasangan material,Jangan meninggalkan mesin yangs sedang beroperasi tanpa pengawasan,Pastikan peralatan yang digunakan dalam kondisi bagus,Prosedur tanggap darurat Kebakaran,Pelatihan tanggap darurat kebakaran \r\n', 'Safety Helmet,Safety Shoes,Wearpack,Masker,alarm detektor kebakaran,APAR,Air Hood'),
('CNC Plasma Cutter', 'Listrik Bertegangan Tinggi,Material yang  tajam ,Panas dari plasma,Asap dan gas beracun,Mesin yang beroprasi,Panas dari material\r\n', 'Melakukan perawatan mesin dan grounding secara teratur,Memberi peringatan peggunaan tombol bahaya listrik,Pastikan mesin belum beroperasi pada ssaat pemsangan material,Membuat rencana dan prosedur tanggap darurat Kebakaran,Melakukan pelatihan tanggap darurat kebaran Memasang dan alarm detektor kebakaran,Pastikan mesin beroperasi pada area dengan ventilasi yang memadai,Jangan melakukan pemotongan pada metal yang dilapisi atau mengandung bahan yang menghasilkan asap beracun Kecuali lapisannya telah dibersihkan dari permukaan metal,Jangan meninggalkan mesin yangs sedang beroperasi tanpa pengawasan,Pastikan peralatan yang digunakan dalam kondisi bagus,Jalankan unit fume extraction sebelum memulai proses pemotongan,area kerja bersih dari minyak oli dan bahan yang mudah terbakar,Jangan melakukan pemotongan pada container yang berisi cairan atau gas yang mudah terbakar\r\n', 'Safety Helmet,Safety Shoes,Wearpack,Masker,alarm detektor kebakaran,APAR,Air Hood'),
('Cutting Machine', 'Material,Selang yang melintang,Tekanan gas tidak sesuai,Valve tidak berfungsi,Api,\r\nGas dan asap beracun,Cahaya Ultraviolet,Holder yang panas,Gas dan asap beracun,Selang,Material yang panas,Serpihan material,Kebisingan\r\n', 'Menata Bengkel sesuai dengan 5R,Memastikan selang tidak terinjak,Periksa sambungan selang dan regulator sebelum digunakan,Periksa sambungan selang dan regulator,Membuat bilik untuk setiap mesin las,Menggunakan alat angkat jika benda terlalu berat,Menggulung selang dengan hati-hati,jauhkan dari bahan-bahan mudah terbakar,berhati-hati pada saat mengelas,Memasang Blower,Menggunakan masker,Memastikan tameng las dalam keadaan baik,Menggulung selang dengan hati-hati,Memastikan selang tidak terinjak,Membersihkan material dengan berlawanan arah dengan badan\r\n\r\n', 'Sarung tangan Las,Wearpack,Safety Shoes,Masker,Tameng Las,Apron Las'),
('EDM', 'Material yang berat,Listrik Bertegangan Tinggi,Wire Electrode yang panas,Percikan Api,Mesin yang beroprasi,Material yang panas\r\n', 'Pastikan benda kerja telah terpasang dengan kuat pada meja,Melakukan perawatan mesin dan grounding secara teratur,Memberi peringatan peggunaan tombol bahaya listrik,Gunakan machining parameters sesuai dengan diameter dan jenis wire electrode serta material benda kerja yang akan di proses,Jangan melakukan pengukuaran benda kerja saat mesin sedang beroperasi,Prosedur tanggap darurat Kebakaran  Pelatihan tanggap darurat kebaran,jangan biarkan orang lain menyentuhnya dan jangan tinggalkan mesin tanpa pengawasan,Menggunakan alat bantu untuk mengambil material,Pastikan wire electrode bebas dari benda kerja sekitar 1-2 mm sebelum memulai menjalankan program,Memasang barrier pada mesin,Pastikan mesin belum beroperasi pada ssaat pemsangan material\r\n', 'Safety Helmet,Safety Shoes,Wearpack,Masker,alarm detektor kebakaran,APAR,Air Hood\r\n'),
('Hacksaw Machine', 'Material yang berat,Listrik Bertegangan Tinggi,Benda tajam,Material yang panas,Percikan api,Pisau,Tombol OFF tidak berfungsi\r\n', 'Menggunakan Safety Shoes,Memastikan isolasi listrik terpasang dengan sempurna,Memastikan bilah gergaji terpasang dengan benar,periksa sistem aliran pendingin dan aliran pendingin mengalir langsung ke bilah gergaji,Melakukan perawatan mesin secara teratur,Memastikan bilah gergaji terpasang dengan benar,Memberi petunjuk peggunaan tombol emergency,Menggunakan alat berat jika benda terlalu berat,Melakukan perawatan mesin dan grounding secara teratur,Memasang barrier,Memberi peringatan peggunaan tombol bahaya listrik,Memasang barrier penghalang,Menjauhkan mesin dari bahan mudah terbakar,Menambahkan pemakanan dengan hati hati,Memasang tombol Emergency\r\n\r\n\r\n\r\n\r\n', 'Sarung tangan,Wearpack,Safety Shoes'),
('Horizontal Band Saw Machine', 'Material yang berat,Ragum yang bergerak,Kran yang kurang baik,Pita gergaji yang tajam,Serpihan,Panas,Percikan api,Pita gergaji yang tajam\r\n', 'Menggunakan Safety Shoes,Mengangkat Lengan dengan hati hati,Memasang barrier penghalang,Menata Bengkel sesuai dengan 5R,pastikan kecepatan ketegangan dan pengarah pita gergaji disesuaikan dengan benar,Periksa apakah pita gergaji dalam kondisi baik,periksa sistem pendingin bekerja dengan baik,Tidak memegang material saat proses pemotongan,Meakuakn pemeriksaan kran scara berkala,jangan mendekati gergaji sampai bilah gergaji benar benar berhenti,Menggunakan alat angkat jika benda terlalu berat,Menjauhkan mesin dari bahan mudah terbakar,Memasang poster peringatan bahaya,Membuat Rem tambahan\r\n\r\n\r\n', 'Sarung tangan,Wearpack,Safety Shoes,Makser'),
('Hydrolic Ironworker Machine', 'Listrik Bertegangan Tinggi,Material yang berat,Pedal,Tombol OFF tidak berfungsi', 'Memastikan isolasi listrik terpasang dengan sempurna,Menata Bengkel sesuai dengan 5R,Memberi peringatan peggunaan tombol bahaya listrik,Memasang tombol Emergency,Memastikan isolasi listrik terpasang dengan sempurna,Menata Bengkel sesuai dengan 5R,Memberi peringatan peggunaan tombol bahaya listrik,Memasang tombol Emergency', 'Sarung tangan,Wearpack,Safety Shoes'),
('Hydrolic Shearing Machine', 'Material yang berat,Listrik Bertegangan Tinggi,Benda tajam,Serpihan,Panas\r\n', 'Memberi peringatan peggunaan tombol bahaya listrik,Memastikan isolasi listrik terpasang dengan sempurna,Menata Bengkel sesuai dengan 5R,pastikan kecepatan ketegangan dan pengarah pita gergaji disesuaikan dengan benar,Periksa apakah pita gergaji dalam kondisi baik,jangan menyentuh matrial saat melakukan pemotongan,periksa sistem pendingin bekerja dengan baik,Menggunakan alat angkat jika benda terlalu berat,Melakukan perawatan mesin dan grounding secara teratur,jauhkan tangan dari bagian pemotongan,Jepit material dengan hati-hati \r\n', 'Sarung tangan,Wearpack,Safety Shoes'),
('Mesin Las OAW', 'Benda kerja yang berat,Sedang Melintang,Tekanan yang berlebih,Valve tidak berfungsi,Gas dan asap beracun,Panas holder dan filler,gas dan asap beracun,Cahaya Ultraviolet,Kebisingan,Api,Material yang panas,Serpihan material yang terbang', 'Memastikan tangan agar tidak licin,Jauhkan dari bahan mudah terbakar,Jauhkan dari bahan mudah terbakar,Memasang dan alarm detektor kebakaran,Melakukan pelatihan tanggap darurat kebaran,Membuat rencana dan prosedur tanggap darurat,Memasang Blower,Menggunakan masker,Memastikan tameng las dalam keadaan baik,\r\nMembuat bilik untuk setiap mesin las,Memasang Blower,Menggunakan masker,Memastikan tameng las dalam keadaan baik,\r\nMembuat bilik untuk setiap mesin las,berhati-hati pada saat bekerja,Membersihkan material dengan berlawanan arah dengan badan', 'Sarung tangan Las,Wearpack,Safety Shoes,Masker,Kaca Mata Las Las,Apron Las,Earmuff,Penjepit'),
('Mesin Las SMAW', 'Listrik Bertegangan Tinggi,Listrik Bertegangan Tinggi,Material yang berat,Api,Cahaya Ultraviolet,Gas dan asap yang beracun,Material yang panas,Debu,Serpihan material,Kebisingan,Meja kerja yang panas,Debu', 'Memastikan isolasi listrik terpasang dengan sempurna,Melakukan perawatan mesin secara teratur,Melakukan pengecekan grounding secara berkala,Memberi peringatan peggunaan tombol,Membuat rencana dan prosedur tanggap darurat,memastikan jalan bebas dari benda-benda,Mengganti sarung tanga lama yan sudah tidak layak pakai,Kebakaran Melakukan pelatihan tanggap darurat kebaran,Memastikan tameng las dalam keadaan baik,Memasang Blower, Menggunakan masker,memastikan material tidak terlalu panas,Memasang dan alarm detektor kebakaran,Menyediaan masker untuk   teknisi,Lakukan pembersihan arah menjauuh dari tubuh,Memastikan meja sudah benar-benar dingin', 'Sarung tangan Las,Wearpack,Safety Shoes,Masker,Tameng Las,Apron Las,Earmuff,Penjepit');

-- --------------------------------------------------------

--
-- Struktur dari tabel `status`
--

CREATE TABLE IF NOT EXISTS `status` (
`ID` int(10) NOT NULL,
  `ACCKepalaBengkel` tinyint(1) NOT NULL,
  `ACCKetuaBJP` tinyint(1) NOT NULL,
  `ACCKetuaUPIK3L` tinyint(1) NOT NULL,
  `NamaBengkel` varchar(50) NOT NULL,
  `ttdBengkel` varchar(30) NOT NULL,
  `NamaBJP` varchar(50) NOT NULL,
  `ttdBJP` varchar(30) NOT NULL,
  `NamaUPIK3L` varchar(50) NOT NULL,
  `ttdUPIK3L` varchar(30) NOT NULL,
  `Waktu` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `status`
--

INSERT INTO `status` (`ID`, `ACCKepalaBengkel`, `ACCKetuaBJP`, `ACCKetuaUPIK3L`, `NamaBengkel`, `ttdBengkel`, `NamaBJP`, `ttdBJP`, `NamaUPIK3L`, `ttdUPIK3L`, `Waktu`) VALUES
(1, 1, 1, 1, 'Harianto', 'defaultttd.jpg', 'Sutrisno', 'defaultttd.jpg', 'Bambang', 'defaultttd.jpg', '2019-06-25 00:40:15'),
(2, 1, 1, 1, 'Yoshi', 'defaultttd.jpg', 'Sutrisno', 'defaultttd.jpg', 'Bambang', 'defaultttd.jpg', '2019-06-25 00:45:29');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tabel1`
--

CREATE TABLE IF NOT EXISTS `tabel1` (
`ID` int(20) NOT NULL,
  `Worktype` varchar(20) NOT NULL,
  `Tanggal` varchar(20) NOT NULL,
  `Lokasi` varchar(20) NOT NULL,
  `Deskripsi` varchar(50) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `tabel1`
--

INSERT INTO `tabel1` (`ID`, `Worktype`, `Tanggal`, `Lokasi`, `Deskripsi`) VALUES
(1, 'Cold Work', '2018-03-05', 'Konstruksi', 'Mengelas Kapal'),
(2, 'Cold Work', '2018-03-05', 'CNC', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tabel2`
--

CREATE TABLE IF NOT EXISTS `tabel2` (
`ID` int(20) NOT NULL,
  `Nama` longtext NOT NULL,
  `Nip` longtext NOT NULL,
  `Bengkel` varchar(20) NOT NULL,
  `Id_user` int(10) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data untuk tabel `tabel2`
--

INSERT INTO `tabel2` (`ID`, `Nama`, `Nip`, `Bengkel`, `Id_user`) VALUES
(1, ',Fahmi,,,', ',617181,,,', 'Konstruksi', 0),
(2, ',Bobby,,,', ',516151,,,', 'CNC', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id` int(10) NOT NULL,
  `username` varchar(20) NOT NULL,
  `Nama` varchar(50) NOT NULL,
  `password` varchar(20) NOT NULL,
  `Jabatan` varchar(20) NOT NULL,
  `ttd` varchar(20) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `username`, `Nama`, `password`, `Jabatan`, `ttd`) VALUES
(2, 'Bengkel', 'Hariyono', 'admin', 'Bengkel', 'defaultttd.jpg'),
(3, 'BJP', 'Sutrisno', 'admin', 'BJP', 'defaultttd.jpg'),
(4, 'UPIK3L', 'Bambang', 'admin', 'UPIK3L', 'defaultttd.jpg'),
(5, 'Admin', 'Jumali', 'admin', 'Admin', 'defaultttd.jpg'),
(9, 'gunawan', 'Gunawan', '123456', 'Teknisi', 'defaultttd.jpg'),
(10, 'KalabCNC', 'Yoshi', 'admin', 'CNC', 'defaultttd.jpg'),
(11, 'KalabKonstruksi', 'Harianto', 'admin', 'Konstruksi', 'defaultttd.jpg'),
(12, 'fahmisyaifudin', 'Fahmi', '123456', 'Teknisi', 'Service_Note.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bengkel`
--
ALTER TABLE `bengkel`
 ADD PRIMARY KEY (`ID`), ADD KEY `PeralatanYangDipakai` (`PeralatanYangDipakai`);

--
-- Indexes for table `peralatan`
--
ALTER TABLE `peralatan`
 ADD PRIMARY KEY (`PeralatanYangDipakai`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tabel1`
--
ALTER TABLE `tabel1`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tabel2`
--
ALTER TABLE `tabel2`
 ADD PRIMARY KEY (`ID`), ADD KEY `Bengkel` (`Bengkel`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bengkel`
--
ALTER TABLE `bengkel`
MODIFY `ID` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tabel1`
--
ALTER TABLE `tabel1`
MODIFY `ID` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tabel2`
--
ALTER TABLE `tabel2`
MODIFY `ID` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
